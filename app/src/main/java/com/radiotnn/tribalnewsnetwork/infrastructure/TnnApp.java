package com.radiotnn.tribalnewsnetwork.infrastructure;

import android.app.Application;

import com.google.android.gms.ads.MobileAds;
import com.radiotnn.tribalnewsnetwork.utilz.FontsOverride;

/**
 * Created by samar_000 on 12/15/2016.
 */

public class TnnApp extends Application {



    @Override
    public void onCreate() {
        super.onCreate();
        FontsOverride.setDefaultFont(this, "DEFAULT", "nafeesweb.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "nafeesweb.ttf");
        FontsOverride.setDefaultFont(this, "SERIF", "nafeesweb.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "nafeesweb.ttf");


    }
}
