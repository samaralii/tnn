package com.radiotnn.tribalnewsnetwork.infrastructure;

import com.radiotnn.tribalnewsnetwork.pojos.DetailPOJO.DetailPOJO;
import com.radiotnn.tribalnewsnetwork.pojos.Itemspojo.ItemsPojo;
import com.radiotnn.tribalnewsnetwork.pojos.RecentPosts;
import com.radiotnn.tribalnewsnetwork.pojos.RelatedPOJO;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by samar_000 on 12/15/2016.
 */

public interface WebServices {


    @FormUrlEncoded
    @POST("get_recent_posts/")
    Call<RecentPosts> getRecentPosts(@Field("token") String token);

    @FormUrlEncoded
    @POST("get_post_detail")
    Call<DetailPOJO> getPostDetail(@Field("token") String token,
                                   @Field("id") String id);

    @FormUrlEncoded
    @POST("get_rel_posts")
    Call<RelatedPOJO> getRelPosts(@Field("token") String token,
                                  @Field("cat_id") String cat_id,
                                  @Field("post_id") String post_id);

    @FormUrlEncoded
    @POST("get_cat_posts")
    Call<RecentPosts> getCatPosts(@Field("token") String token,
                                  @Field("id") String id);


    @FormUrlEncoded
    @POST("get_category_list")
    Call<ItemsPojo> getItems(@Field("token") String token);


}
