package com.radiotnn.tribalnewsnetwork.utilz;

import retrofit2.Response;

/**
 * Created by samar_000 on 12/15/2016.
 */

public class Listeners {

    public interface DialogListener {
        void OnOkClick();
    }


    public interface Call<T> {
        void CallInstance(retrofit2.Call call);
        void onSuccess(Response<T> response);
        void onError();
    }

}
