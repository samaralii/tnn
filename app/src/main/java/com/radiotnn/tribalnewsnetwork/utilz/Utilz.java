package com.radiotnn.tribalnewsnetwork.utilz;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

/**
 * Created by samar_000 on 12/15/2016.
 */

public class Utilz {

    public static void tmsg(Activity activity, String message) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    public static Retrofit getRetrofit(String url) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        return new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public static Retrofit getRetrofit() {
        return getRetrofit(AppConstant.BASE_URL);
    }

    public static void showAlert(Context c, String t, String m) {
        AlertDialog dialog = new AlertDialog.Builder(c).
                setTitle(t)
                .setMessage(m)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create();
        dialog.show();

    }

    public static void showAlert(Context c, String t, String m, final Listeners.DialogListener listener) {
        AlertDialog dialog = new AlertDialog.Builder(c).
                setTitle(t)
                .setMessage(m)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.OnOkClick();
                        dialogInterface.dismiss();
                    }
                })
                .create();
        dialog.show();

    }

    public static String getDateFormat(String DateTime) {

        checkNotNull(DateTime);

        String dateTime[] = DateTime.split(" ");

        String date = dateTime[0];
        String time = dateTime[1];

        String longDate[] = date.split("-");
        String year = longDate[0];
        String month = longDate[1];
        String day = longDate[2];

//        return day + "  " + getMonthInUrdu(Integer.parseInt(month)) + "  " + year + "  " + time;
        return day + "  " + getMonthInUrdu(Integer.parseInt(month)) + "  " + year;
    }

    private static String boldText(String text) {
        return String.valueOf(Html.fromHtml("<b>Title</b>: " + text));
    }

    private static String getMonth(int month) {

        switch (month) {
            case 1:
                return "Jan";
            case 2:
                return "Feb";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "Sept";
            case 10:
                return "Oct";
            case 11:
                return "Nov";
            case 12:
                return "Dec";
            default:
                return "NNN";
        }

    }

    private static String getMonthInUrdu(int month) {

        switch (month) {
            case 1:
                return "جنوری";
            case 2:
                return "فروري";
            case 3:
                return "مارچ";
            case 4:
                return "اپريل";
            case 5:
                return "مئ";
            case 6:
                return "جون";
            case 7:
                return "جولائ";
            case 8:
                return "اگست";
            case 9:
                return "ستمبر";
            case 10:
                return "اکتوبر";
            case 11:
                return "نومبر";
            case 12:
                return "دسمبر";
            default:
                return "NNN";
        }

    }

    public static String replaceArabicNumbers(String original) {
        return original.replaceAll("1", "١")
                .replaceAll("2", "٢")
                .replaceAll("3", "٣")
                .replaceAll("4", "۴")
                .replaceAll("5", "۵")
                .replaceAll("6", "۶")
                .replaceAll("7", "۷")
                .replaceAll("8", "۸")
                .replaceAll("9", "۹")
                ;
    }

    public class AppConstant {
        public static final String BASE_URL = "https://radiotnn.com/urdu/api/";

    }


}