package com.radiotnn.tribalnewsnetwork;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.radiotnn.tribalnewsnetwork.infrastructure.WebServices;
import com.radiotnn.tribalnewsnetwork.pojos.DetailPOJO.DetailPOJO;
import com.radiotnn.tribalnewsnetwork.pojos.Itemspojo.ItemsPojo;
import com.radiotnn.tribalnewsnetwork.pojos.RecentPosts;
import com.radiotnn.tribalnewsnetwork.pojos.RelatedPOJO;
import com.radiotnn.tribalnewsnetwork.utilz.Listeners;
import com.radiotnn.tribalnewsnetwork.utilz.Utilz;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by samar_000 on 12/15/2016.
 */

public class Apis {

    private static final String TAG = Apis.class.getName().toUpperCase();
    private static final String TOKEN = "TNN218";


    public static void getRecentPosts(Context context, final Listeners.Call<RecentPosts> callBacks) {

        WebServices api = Utilz.getRetrofit().create(WebServices.class);
        final ProgressDialog dialog = ProgressDialog.show(context, "", "Loading News");


        api.getRecentPosts(TOKEN).enqueue(new Callback<RecentPosts>() {
            @Override
            public void onResponse(Call<RecentPosts> call, Response<RecentPosts> response) {

                try {

                    if (response == null || !response.isSuccessful()) {
                        callBacks.onError();
                    dialog.dismiss();
                    }

                    callBacks.onSuccess(response);
                dialog.dismiss();

                } catch (Exception e) {
                    Log.e(TAG, "onResponse: ", e);
                    callBacks.onError();
                    dialog.dismiss();

                }


            }

            @Override
            public void onFailure(Call<RecentPosts> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                callBacks.onError();
                dialog.dismiss();

            }
        });

    }


    public static void getPostDetail(Context context, String id, final Listeners.Call<DetailPOJO> callback) {

        WebServices api = Utilz.getRetrofit().create(WebServices.class);
        final ProgressDialog dialog = ProgressDialog.show(context, "", "Loading News");


        api.getPostDetail(TOKEN, id).enqueue(new Callback<DetailPOJO>() {
            @Override
            public void onResponse(Call<DetailPOJO> call, Response<DetailPOJO> response) {

                if (response == null || !response.isSuccessful()) {
                    callback.onError();
                    dialog.dismiss();
                }

                callback.onSuccess(response);
                dialog.dismiss();


            }

            @Override
            public void onFailure(Call<DetailPOJO> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                callback.onError();
                dialog.dismiss();

            }
        });

    }


    public static void getRelatedPost(String cat_id,
                                      String post_id, final Listeners.Call<RelatedPOJO> callback) {

        WebServices api = Utilz.getRetrofit().create(WebServices.class);

        Call<RelatedPOJO> call = api.getRelPosts(TOKEN, cat_id, post_id);

        callback.CallInstance(call);

        call.enqueue(new Callback<RelatedPOJO>() {
            @Override
            public void onResponse(Call<RelatedPOJO> call, Response<RelatedPOJO> response) {
                if (response == null || !response.isSuccessful()) {
                    callback.onError();
                }

                callback.onSuccess(response);
            }

            @Override
            public void onFailure(Call<RelatedPOJO> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                callback.onError();
            }
        });
    }

    public static void getCatPosts(Context context, String id, final Listeners.Call<RecentPosts> callBack) {

        WebServices api = Utilz.getRetrofit().create(WebServices.class);
        final ProgressDialog dialog = ProgressDialog.show(context, "", "Loading News");

        api.getCatPosts(TOKEN, id).enqueue(new Callback<RecentPosts>() {
            @Override
            public void onResponse(Call<RecentPosts> call, Response<RecentPosts> response) {


                try {

                    if (response == null || !response.isSuccessful()) {
                        callBack.onError();
                        dialog.dismiss();
                    }

                    callBack.onSuccess(response);
                    dialog.dismiss();


                } catch (Exception e) {
                    Log.e(TAG, "onFailure: ", e);
                    callBack.onError();
                    dialog.dismiss();
                }



            }

            @Override
            public void onFailure(Call<RecentPosts> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                callBack.onError();
                dialog.dismiss();

            }
        });

    }


    public static void getItems(Context context, final Listeners.Call<ItemsPojo> callBack) {
        WebServices api = Utilz.getRetrofit().create(WebServices.class);
//        final ProgressDialog dialog = ProgressDialog.show(context, "", "Loading");

        api.getItems(TOKEN).enqueue(new Callback<ItemsPojo>() {
            @Override
            public void onResponse(Call<ItemsPojo> call, Response<ItemsPojo> response) {
                if (response == null || !response.isSuccessful()) {
                    callBack.onError();
//                    dialog.dismiss();
                }

                callBack.onSuccess(response);
//                dialog.dismiss();

            }

            @Override
            public void onFailure(Call<ItemsPojo> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                callBack.onError();
//                dialog.dismiss();
            }
        });
    }

}
