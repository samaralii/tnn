package com.radiotnn.tribalnewsnetwork.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.radiotnn.tribalnewsnetwork.R;
import com.radiotnn.tribalnewsnetwork.pojos.Post;
import com.radiotnn.tribalnewsnetwork.utilz.Utilz;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by samar_000 on 12/29/2016.
 */

public class Post_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private List<Post> posts;
    private Context context;
    private Post headerPost;
    private ClickListener listener;

    public Post_Adapter(List<Post> posts, Context context,
                        Post headerPost, ClickListener listener) {
        this.posts = posts;
        this.context = context;
        this.headerPost = headerPost;
        this.listener = listener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_item, parent, false);
            return new VHHeader(v);
        } else if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_recent_post, parent, false);
            return new VHItem(v);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType
                + " + make sure your using types correctly");

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof VHHeader) {

            Post p = headerPost;
            VHHeader VHheader = (VHHeader) holder;
            Picasso.with(context).load(p.getFeatured_img()).into(VHheader.headerImage);

            VHheader.tvHeaderTitle.setText(p.getPostTitle());


            String timeFormat = Utilz.getDateFormat(posts.get(position).getPostDate());

            VHheader.tvHeaderTime.setText(timeFormat);
//            VHheader.tvHeaderTime.setText(p.getPostDate());


        } else if (holder instanceof VHItem) {

            Post p = posts.get(position);
            VHItem vhItem = (VHItem) holder;


            vhItem.tvText.setText(posts.get(position).getPostTitle());

            String timeFormat = Utilz.getDateFormat(posts.get(position).getPostDate());

            vhItem.tvTime.setText(timeFormat);


//            vhItem.tvTime.setText(Utilz.getDateFormat(posts.get(position).getPostDate()));

            Picasso.with(context).load(posts.get(position).getFeatured_img()).into(vhItem.imageView);


        }
    }


    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }


    @Override
    public int getItemCount() {
        return posts.size();
    }


    public interface ClickListener {
        void onClick(Post posts);
    }

    public class VHHeader extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.main_fragment_headerImage)
        ImageView headerImage;
        @BindView(R.id.main_fragment_headerTitle)
        TextView tvHeaderTitle;
        @BindView(R.id.main_fragment_headerTime)
        TextView tvHeaderTime;


        public VHHeader(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onClick(headerPost);
        }
    }

    public class VHItem extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.list_recent_post_image)
        ImageView imageView;
        @BindView(R.id.list_recent_post_text)
        TextView tvText;
        @BindView(R.id.list_recent_post_time)
        TextView tvTime;


        public VHItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onClick(posts.get(getLayoutPosition()));
        }
    }


}
