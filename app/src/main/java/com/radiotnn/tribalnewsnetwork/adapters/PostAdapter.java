package com.radiotnn.tribalnewsnetwork.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.radiotnn.tribalnewsnetwork.R;
import com.radiotnn.tribalnewsnetwork.pojos.Post;
import com.radiotnn.tribalnewsnetwork.utilz.Utilz;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by samar_000 on 12/15/2016.
 */

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostViewHolder> {


    private List<Post> posts;
    private Context context;
    private ClickListener listener;

    public PostAdapter(List<Post> posts, ClickListener listener) {
        this.posts = posts;
        this.listener = listener;
    }


    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new PostViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_recent_post, parent, false));
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {

        holder.tvText.setText(posts.get(position).getPostTitle());

        String timeFormat = Utilz.getDateFormat(posts.get(position).getPostDate());

        holder.tvTime.setText(timeFormat);

        Picasso.with(context).load(posts.get(position).getFeatured_img()).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public interface ClickListener {
        void onClick(Post posts);
    }

    class PostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.list_recent_post_image)
        ImageView imageView;
        @BindView(R.id.list_recent_post_text)
        TextView tvText;
        @BindView(R.id.list_recent_post_time)
        TextView tvTime;

        PostViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            listener.onClick(posts.get(getLayoutPosition()));
        }
    }
}
