
package com.radiotnn.tribalnewsnetwork.pojos.Itemspojo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemsPojo {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("count")
    @Expose
    private int count;
    @SerializedName("count_total")
    @Expose
    private int countTotal;
    @SerializedName("pages")
    @Expose
    private int pages;
    @SerializedName("posts")
    @Expose
    private List<Post> postItems = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCountTotal() {
        return countTotal;
    }

    public void setCountTotal(int countTotal) {
        this.countTotal = countTotal;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public List<Post> getPostItems() {
        return postItems;
    }

    public void setPostItems(List<Post> postItems) {
        this.postItems = postItems;
    }

}
