
package com.radiotnn.tribalnewsnetwork.pojos.Itemspojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Post {

    @SerializedName("term_id")
    @Expose
    private String termId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("term_group")
    @Expose
    private String termGroup;
    @SerializedName("term_taxonomy_id")
    @Expose
    private String termTaxonomyId;
    @SerializedName("taxonomy")
    @Expose
    private String taxonomy;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("parent")
    @Expose
    private String parent;
    @SerializedName("count")
    @Expose
    private String count;
    @SerializedName("cat_ID")
    @Expose
    private String catID;
    @SerializedName("category_count")
    @Expose
    private String categoryCount;
    @SerializedName("category_description")
    @Expose
    private String categoryDescription;
    @SerializedName("cat_name")
    @Expose
    private String catName;
    @SerializedName("category_nicename")
    @Expose
    private String categoryNicename;
    @SerializedName("category_parent")
    @Expose
    private String categoryParent;

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTermGroup() {
        return termGroup;
    }

    public void setTermGroup(String termGroup) {
        this.termGroup = termGroup;
    }

    public String getTermTaxonomyId() {
        return termTaxonomyId;
    }

    public void setTermTaxonomyId(String termTaxonomyId) {
        this.termTaxonomyId = termTaxonomyId;
    }

    public String getTaxonomy() {
        return taxonomy;
    }

    public void setTaxonomy(String taxonomy) {
        this.taxonomy = taxonomy;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getCatID() {
        return catID;
    }

    public void setCatID(String catID) {
        this.catID = catID;
    }

    public String getCategoryCount() {
        return categoryCount;
    }

    public void setCategoryCount(String categoryCount) {
        this.categoryCount = categoryCount;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCategoryNicename() {
        return categoryNicename;
    }

    public void setCategoryNicename(String categoryNicename) {
        this.categoryNicename = categoryNicename;
    }

    public String getCategoryParent() {
        return categoryParent;
    }

    public void setCategoryParent(String categoryParent) {
        this.categoryParent = categoryParent;
    }

}
