package com.radiotnn.tribalnewsnetwork.pojos;

import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;

/**
 * Created by samar_000 on 12/29/2016.
 */

public class TestPojo {
    private String id;
    private PrimaryDrawerItem item;
    private String name;

    public TestPojo(String id, PrimaryDrawerItem item, String name) {
        this.id = id;
        this.item = item;
        this.name = name;
    }

    public TestPojo() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PrimaryDrawerItem getItem() {
        return item;
    }

    public void setItem(PrimaryDrawerItem item) {
        this.item = item;
    }
}
