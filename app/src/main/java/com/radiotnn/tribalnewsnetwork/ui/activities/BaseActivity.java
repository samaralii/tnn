package com.radiotnn.tribalnewsnetwork.ui.activities;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.radiotnn.tribalnewsnetwork.R;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

/**
 * Created by samar_000 on 12/15/2016.
 */

public class BaseActivity extends AppCompatActivity {


    protected Toolbar toolbar;


    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        InitializeToolbar();
    }


    void InitializeToolbar() {
        toolbar = (Toolbar) findViewById(R.id.app_toolbar);
        if (toolbar != null)
            setSupportActionBar(toolbar);
    }


    void openFragment(Fragment fragment, @IdRes int IdRes) {

        checkNotNull(fragment);
        checkNotNull(IdRes);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(IdRes, fragment);
        ft.commit();

    }

    void openFragment(Fragment fragment, @IdRes int IdRes, Bundle b) {

        checkNotNull(fragment);
        checkNotNull(IdRes);
        checkNotNull(b);


        fragment.setArguments(b);

        openFragment(fragment, IdRes);

    }

    public void changeTitle(String title) {
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(title);

    }


}
