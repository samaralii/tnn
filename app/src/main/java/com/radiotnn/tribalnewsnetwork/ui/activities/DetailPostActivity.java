package com.radiotnn.tribalnewsnetwork.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;

import com.radiotnn.tribalnewsnetwork.R;
import com.radiotnn.tribalnewsnetwork.ui.fragments.DetailFragment;

/**
 * Created by samar_000 on 12/28/2016.
 */

public class DetailPostActivity extends BaseActivity {


    public static final String TITLE = "title";
    public static final String POST_ID = "postid";
    public static final String CAT_ID = "catId";

    private String postId;
    private String catId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);
        String title = getIntent().getStringExtra(TITLE);
        postId = getIntent().getStringExtra(POST_ID);
        catId = getIntent().getStringExtra(CAT_ID);

//        if (getSupportActionBar() != null)
//            getSupportActionBar().setTitle("");


        init();

    }

    private void init() {

        Bundle b = new Bundle();
        b.putString(POST_ID, postId);
        b.putString(CAT_ID, catId);
        openFragment(new DetailFragment(), R.id.detail_activity_container, b);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_detail, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_cancel) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
