package com.radiotnn.tribalnewsnetwork.ui.fragments;

import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.radiotnn.tribalnewsnetwork.Apis;
import com.radiotnn.tribalnewsnetwork.R;
import com.radiotnn.tribalnewsnetwork.adapters.PostAdapter;
import com.radiotnn.tribalnewsnetwork.pojos.DetailPOJO.DetailPOJO;
import com.radiotnn.tribalnewsnetwork.pojos.Post;
import com.radiotnn.tribalnewsnetwork.pojos.RelatedPOJO;
import com.radiotnn.tribalnewsnetwork.ui.activities.DetailPostActivity;
import com.radiotnn.tribalnewsnetwork.ui.activities.MainActivity;
import com.radiotnn.tribalnewsnetwork.utilz.Listeners;
import com.radiotnn.tribalnewsnetwork.utilz.Utilz;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by samar_000 on 12/28/2016.
 */

public class DetailFragment extends BaseFragment implements PostAdapter.ClickListener {


    private static final String TAG = DetailFragment.class.getName().toUpperCase();
    @BindView(R.id.detail_fragment_imageView)
    ImageView imageView;
    @BindView(R.id.detail_fragment_title)
    TextView tvTitle;
    @BindView(R.id.detail_fragment_time)
    TextView tvTime;
    @BindView(R.id.detail_fragment_detailText)
    TextView tvDetailText;
    @BindView(R.id.detail_fragment_recycleView)
    RecyclerView recyclerView;
    @BindView(R.id.detail_fragment_progressbar)
    ProgressBar progressBar;

    @BindView(R.id.detail_fragment_commentWebView)
    WebView webView;
    //    @BindView(R.id.detail_fragment_commentWebView)
    WebView mWebviewPop;

    @BindView(R.id.adView)
    FrameLayout mContainer;

    private Unbinder unbinder;
    private String postId;
    private String catId;
    private Call mCall;
    private String postUrl;

    boolean isLoading;


    @BindView(R.id.progressbar)
    ProgressBar progressBarr;
    private String posturlToShare;

    private String newsTitle = "";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        postId = getArguments().getString(DetailPostActivity.POST_ID);
        catId = getArguments().getString(DetailPostActivity.CAT_ID);
        ((DetailPostActivity)getActivity()).changeTitle("");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.detail_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        init();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    private void init() {
        getPostDetail();
    }

    private void getPostDetail() {

        Apis.getPostDetail(getContext(), postId, new Listeners.Call<DetailPOJO>() {
            @Override
            public void CallInstance(Call call) {

            }

            @Override
            public void onSuccess(Response<DetailPOJO> response) {

                if (response.body().getStatus().equalsIgnoreCase("ok")) {


                    try {
                        Picasso.with(getContext()).load(response.body().getPosts().getFeaturedImg().get(0)).into(imageView);
                    } catch (Exception e) {
                        Log.e(TAG, "onSuccess: ", e);
                    }


                    tvTitle.setText(response.body().getPosts().getPostTitle());
                    newsTitle = response.body().getPosts().getPostTitle();


                    String timeFormat = Utilz.getDateFormat(response.body().getPosts().getPostDate());

                    tvTime.setText(timeFormat);

                    String postDetailText =
                            String.valueOf(Html.fromHtml(response.body().getPosts().getPostContent()));
                    tvDetailText.setText(postDetailText);


                    String title = response.body().getPosts().getCat_name();
                    ((DetailPostActivity)getActivity()).changeTitle(title);



                    postUrl = response.body().getPosts().getCustom_url();
                    posturlToShare = response.body().getPosts().getGuid();
                    progressBarr.setVisibility(View.VISIBLE);
                    setLoading(true);
                    loadComments();


                    try {
                        getRelatedPost();
                    } catch (Exception e) {
                        Log.e("ERROR", "onSuccess: ", e);
                    }


                } else {
                    Utilz.tmsg(getActivity(), "Error");
                }
            }

            @Override
            public void onError() {
                Utilz.tmsg(getActivity(), "Error");
            }
        });
    }

    private void getRelatedPost() {

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);

        progressBar.setVisibility(View.VISIBLE);

        Apis.getRelatedPost(catId, postId, new Listeners.Call<RelatedPOJO>() {


            @Override
            public void CallInstance(Call call) {
                mCall = call;
            }

            @Override
            public void onSuccess(Response<RelatedPOJO> response) {

                if (response.body().getStatus().equalsIgnoreCase("ok")) {
                    try {
                        recyclerView.setVisibility(View.VISIBLE);
                    } catch (Exception e) {
                        Log.e("ERROR", "onSuccess: ", e);
                    }


                    PostAdapter adapter = new PostAdapter(response.body().getPosts(), DetailFragment.this);

                    try {
                        recyclerView.setAdapter(adapter);

                    } catch (Exception e) {
                        Log.e("ERROR", "onSuccess: ", e);
                    }


                } else {


                    try {
                        Utilz.tmsg(getActivity(), "Error");
                    } catch (Exception e) {
                        Log.e("ERROR", "onSuccess: ", e);
                    }

                }


                try {
                    progressBar.setVisibility(View.GONE);
                } catch (Exception e) {
                    Log.e("ERROR", "onSuccess: ", e);
                }
            }

            @Override
            public void onError() {

                try {
                    progressBar.setVisibility(View.GONE);
                    Utilz.tmsg(getActivity(), "Error");
                } catch (Exception e) {
                    Log.e("ERROR", "onSuccess: ", e);
                }


            }
        });
    }


    private void loadComments() {

        webView.setWebViewClient(new UriWebViewClient());
        webView.setWebChromeClient(new UriChromeClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setBuiltInZoomControls(false);
        CookieManager.getInstance().setAcceptCookie(true);

        if (Build.VERSION.SDK_INT >= 21) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
        }

        // facebook comment widget including the article url
        String html = "<!doctype html> <html lang=\"en\"> <head></head> <body> " +
                "<div id=\"fb-root\"></div> <script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = \"//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6\"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));</script> " +
                "<div class=\"fb-comments\" data-href=\"" + postUrl + "\" " +
                "data-numposts=\"" + "5" + "\" data-order-by=\"reverse_time\">" +
                "</div> </body> </html>";

        webView.loadDataWithBaseURL("http://www.nothing.com", html, "text/html", "UTF-8", null);
        webView.setMinimumHeight(200);
    }


    private class UriWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            String host = Uri.parse(url).getHost();

            return !host.equals("m.facebook.com");

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            String host = Uri.parse(url).getHost();
            setLoading(false);
            if (url.contains("/plugins/close_popup.php?reload")) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        mContainer.removeView(mWebviewPop);
                        loadComments();
                    }
                }, 600);
            }
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler,
                                       SslError error) {
            setLoading(false);
        }
    }


    class UriChromeClient extends WebChromeClient {

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog,
                                      boolean isUserGesture, Message resultMsg) {
            mWebviewPop = new WebView(getActivity().getApplicationContext());
            mWebviewPop.setVerticalScrollBarEnabled(false);
            mWebviewPop.setHorizontalScrollBarEnabled(false);
            mWebviewPop.setWebViewClient(new UriWebViewClient());
            mWebviewPop.setWebChromeClient(this);
            mWebviewPop.getSettings().setJavaScriptEnabled(true);
            mWebviewPop.getSettings().setDomStorageEnabled(true);
            mWebviewPop.getSettings().setSupportZoom(false);
            mWebviewPop.getSettings().setBuiltInZoomControls(false);
            mWebviewPop.getSettings().setSupportMultipleWindows(true);
            mWebviewPop.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            mContainer.addView(mWebviewPop);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(mWebviewPop);
            resultMsg.sendToTarget();

            return true;
        }

        @Override
        public boolean onConsoleMessage(ConsoleMessage cm) {
            Log.i("TAG", "onConsoleMessage: " + cm.message());
            return true;
        }

        @Override
        public void onCloseWindow(WebView window) {
        }
    }


    private void setLoading(boolean isLoading) {
        this.isLoading = isLoading;

        if (isLoading)
            progressBarr.setVisibility(View.VISIBLE);
        else
            progressBarr.setVisibility(View.GONE);

//        getActivity().invalidateOptionsMenu();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mCall.cancel();
    }

    @Override
    public void onStop() {
        super.onStop();
        mCall.cancel();
    }


    @OnClick(R.id.detail_fragment_fab_share)
    void OnShareClick() {


        String shareText = newsTitle + "\n " + posturlToShare;

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));

    }

    @Override
    public void onClick(Post posts) {

        Intent i = new Intent(getActivity(), DetailPostActivity.class);
        i.putExtra(DetailPostActivity.TITLE, "something");
        i.putExtra(DetailPostActivity.POST_ID, posts.getID() + "");
        i.putExtra(DetailPostActivity.CAT_ID, catId);

        startActivity(i);

        getActivity().finish();
    }
}
