package com.radiotnn.tribalnewsnetwork.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.radiotnn.tribalnewsnetwork.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by samar_000 on 12/28/2016.
 */

public class WebViewActivity extends BaseActivity {

    private static String URL = "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/users/159269159&color=ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false";

    @BindView(R.id.webView)
    WebView webView;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view_activity);
        ButterKnife.bind(this);
        init();

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("تازہ ترین بلیٹن");


    }


    private void init() {
        webView.loadUrl(URL);
        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                progressBar.setVisibility(View.GONE);
            }


            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url != null && url.startsWith("intent://")) {
                    String id = "159269159";
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("soundcloud://users:" + id));
                    startActivity(intent);
                    return true;
                } else {
                    return false;
                }
            }

        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_detail, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        webView.destroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_cancel) {
            webView.destroy();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
