package com.radiotnn.tribalnewsnetwork.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.MobileAds;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.radiotnn.tribalnewsnetwork.Apis;
import com.radiotnn.tribalnewsnetwork.R;
import com.radiotnn.tribalnewsnetwork.pojos.Itemspojo.ItemsPojo;
import com.radiotnn.tribalnewsnetwork.pojos.TestPojo;
import com.radiotnn.tribalnewsnetwork.ui.fragments.CatFragment;
import com.radiotnn.tribalnewsnetwork.ui.fragments.MainFragment;
import com.radiotnn.tribalnewsnetwork.utilz.Listeners;
import com.radiotnn.tribalnewsnetwork.utilz.Utilz;

import java.util.ArrayList;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by samar_000 on 12/15/2016.
 */

public class MainActivity extends BaseActivity implements Drawer.OnDrawerItemClickListener {


    protected Drawer rightDrawer;
    ArrayList<TestPojo> items;

    private final static String APP_AD_ID = "ca-app-pub-2345021006346456~2850406128";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        MobileAds.initialize(this, APP_AD_ID);


        ButterKnife.bind(this);

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("صفحۂ اول");


        addDrawer();
        init();
    }


    private void addDrawer() {

        items = new ArrayList<>();


        //Getting drawer items
        Apis.getItems(this, new Listeners.Call<ItemsPojo>() {
            @Override
            public void CallInstance(Call call) {

            }

            @Override
            public void onSuccess(Response<ItemsPojo> response) {

                try {

                    if (response.body().getStatus().equalsIgnoreCase("ok")) {


                        items.add(new TestPojo("1", new PrimaryDrawerItem().withName("صفحۂ اول"), "صفحۂ اول"));
                        items.add(new TestPojo("0", new PrimaryDrawerItem().withName("تازہ ترین بلیٹن").withSelectable(false), "تازہ ترین بلیٹن"));


                        for (int i = 0; i < response.body().getPostItems().size(); i++) {
                            TestPojo testPojo = new TestPojo();
                            testPojo.setId(response.body().getPostItems().get(i).getCatID());
                            testPojo.setItem(new PrimaryDrawerItem().withName(
                                    response.body().getPostItems().get(i).getName()
                            ));
                            testPojo.setName(response.body().getPostItems().get(i).getName());

                            items.add(testPojo);
                        }


                        addItemsToDrawer();

                    } else {
                        Utilz.tmsg(MainActivity.this, "Error");
                        errorActivity();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    errorActivity();
                }


            }

            @Override
            public void onError() {
                errorActivity();
            }
        });

    }

    private void errorActivity() {
        Utilz.tmsg(MainActivity.this, "Error, Please check your internet connection and try again.");
        finish();

    }

    private void addItemsToDrawer() {


        DrawerBuilder db = new DrawerBuilder();
        db.withActivity(this);
        db.withActivity(this);
        db.withHeader(R.layout.drawer_header);
        db.withDrawerGravity(GravityCompat.END);
        db.withDrawerWidthDp(190);
        db.withOnDrawerItemClickListener(this);


        for (int i = 0; i < items.size(); i++) {
            db.addDrawerItems(items.get(i).getItem());
        }

        rightDrawer = db.build();


    }


    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

        if (position == 1) {

            openFragment(new MainFragment(), R.id.activity_main_container);
            changeTitle("صفحۂ اول");

        } else if (position == 2) {

            startActivity(new Intent(this, WebViewActivity.class));

        } else {

            openCatFragment(items.get(position - 1).getId());
            changeTitle(items.get(position - 1).getName());

        }


        return false;
    }


    private void init() {
        openFragment(new MainFragment(), R.id.activity_main_container);
    }


    private void openCatFragment(String cat_id) {
        Bundle b = new Bundle();
        b.putString(CatFragment.CAT_ID, cat_id);
        openFragment(new CatFragment(), R.id.activity_main_container, b);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_openRight) {
            rightDrawer.openDrawer();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
