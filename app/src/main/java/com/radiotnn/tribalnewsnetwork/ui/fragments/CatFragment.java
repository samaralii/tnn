package com.radiotnn.tribalnewsnetwork.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.radiotnn.tribalnewsnetwork.Apis;
import com.radiotnn.tribalnewsnetwork.R;
import com.radiotnn.tribalnewsnetwork.adapters.PostAdapter;
import com.radiotnn.tribalnewsnetwork.adapters.PostAdapterWithAd;
import com.radiotnn.tribalnewsnetwork.pojos.Post;
import com.radiotnn.tribalnewsnetwork.pojos.RecentPosts;
import com.radiotnn.tribalnewsnetwork.ui.activities.DetailPostActivity;
import com.radiotnn.tribalnewsnetwork.utilz.Listeners;
import com.radiotnn.tribalnewsnetwork.utilz.Utilz;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by samar_000 on 12/28/2016.
 */

public class CatFragment extends BaseFragment implements PostAdapter.ClickListener {

    public static final java.lang.String CAT_ID = "cat_id";

    @BindView(R.id.main_fragment_recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private Unbinder unbinder;
    private String catId;
    private List<Post> mList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        catId = getArguments().getString(CAT_ID);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment_2, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        init();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    private void init() {

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                recyclerView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(true);
                _getAllRecentPosts();


            }
        });

        _getAllRecentPosts();
    }

    private void _getAllRecentPosts() {

        mList.clear();

        Apis.getCatPosts(getActivity(), catId, new Listeners.Call<RecentPosts>() {
            @Override
            public void CallInstance(Call call) {

            }

            @Override
            public void onSuccess(final Response<RecentPosts> response) {


                if (response.body().getStatus().equalsIgnoreCase("ok")) {

                    final Post post = response.body().getPosts().get(0);


                    List<Post> list = response.body().getPosts();

//                    list.remove(0);


                    int countMid = list.size() / 2;

                    for (int i = 0; i < list.size(); i++) {

                        if (i == countMid || i == list.size() - 1) {
                            mList.add(null);
                        }

                        mList.add(list.get(i));

                    }

                    PostAdapterWithAd adapter = new PostAdapterWithAd(mList, getContext(), post, new PostAdapterWithAd.ClickListener() {
                        @Override
                        public void onClick(Post posts) {
                            showPostDetail(posts);
                        }
                    });

                    recyclerView.setAdapter(adapter);


                } else {
                    Utilz.tmsg(getActivity(), "No Post Found");
                }


                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onError() {
                Utilz.tmsg(getActivity(), "Error");

                if (swipeRefreshLayout.isRefreshing())
                    swipeRefreshLayout.setRefreshing(false);

            }
        });


    }


    void showPostDetail(Post posts) {
        Intent i = new Intent(getActivity(), DetailPostActivity.class);
        i.putExtra(DetailPostActivity.TITLE, "something");
        i.putExtra(DetailPostActivity.POST_ID, posts.getID() + "");
        i.putExtra(DetailPostActivity.CAT_ID, catId);

        startActivity(i);
    }

    @Override
    public void onClick(Post posts) {
        showPostDetail(posts);
    }
}
